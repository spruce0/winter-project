import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import './plugins/element.js'
import mavonEditor from 'mavon-editor'
import 'mavon-editor/dist/css/index.css'

// 导入全局的 css
import './assets/css/global.css'

import Nprogress from 'nprogress'
import 'nprogress/nprogress.css'

// 导入 axios
import axios from 'axios'
// 默认请求根路径
axios.defaults.baseURL = 'https://cnodejs.org/api/v1/'
axios.interceptors.request.use(config => {
  Nprogress.start()
  Nprogress.set(0.4)
  return config
})

axios.interceptors.response.use(config => {
  Nprogress.done()
  return config
})
// 设置 跨域
// axios.defaults.withCredentials = true
// axios.interceptor.requset.use(config => { return config })
Vue.prototype.$http = axios

Vue.use(mavonEditor)

// 全局过滤器 过滤分类
Vue.filter('formatClass', function (tab) {
  switch (tab) {
    case 'share':
      return '分享'
    case 'ask':
      return '问答'
    case 'good':
      return '精华'
    case 'job':
      return '招聘'
    default:
      return '分享'
  }
})

Vue.filter('formatCDate', function (time) {
  return time.replace(/T/g, ' ').replace(/\.[\d]{3}Z/, '')
})

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
