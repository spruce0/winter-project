const Storage = {}
const Session = {}

Storage.set = function (key, value) {
  window.localStorage.setItem(key, value)
}
Storage.get = function (key) {
  return window.localStorage.getItem(key)
}

Session.set = function (key, value) {
  window.sessionStorage.setItem(key, value)
}
Session.get = function (key) {
  return window.sessionStorage.getItem(key)
}

export { Storage, Session }
